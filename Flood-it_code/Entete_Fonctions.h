#ifndef __ENTETE_FONCTIONS__
#define __ENTETE_FONCTIONS__

#include "API_Grille.h"
#include "Liste_case.h"


typedef struct _t{
	int nbcl; 			/* nombre de couleurs */
	int dim;				/* dimension de la grille */
	
	ListeCase Lzsg; /* Liste des cases de la Zsg */
	ListeCase *B; 	/* Tableau de case de taille nbcl des cases bordant la Zsg */
	int **App; 			/* Tableau à double entrée donnant l'appartenance des case(i, j) (default = -2, Zsg = -1, Bordure = cl)*/
} S_zsg;

/* Retourne dans L la liste des cases de meme couleur que la case i,j
   et met -1 dans ces cases */
void trouve_zone_rec(int **M, int nbcase,int i, int j, int *taille, ListeCase *L);

/* Algorithme tirant au sort une couleur: il utilise la fonction recursive pour determiner la Zsg */
int sequence_aleatoire_rec(int **M, Grille *G, int dim, int nbcl, int aff);



void init_Zsg(S_zsg *z, int dim, int nbcl);

void ajoute_Zsg(S_zsg *z, int i, int j);

void ajoute_Bordure(S_zsg *z, int i, int j, int cl);

int appartient_Zsg(S_zsg *z, int i, int j);

int appartient_Bordure(S_zsg *z, int i, int j, int cl);

int est_dans_grille(S_zsg *z, int i, int j);

int caseAdjacente(int **M, S_zsg *z, int cl, int i, int j, ListeCase *L);

int agrandit_Zsg(int **M, S_zsg *z, int cl, int k, int l);


int sequence_aleatoire_rapide(int **M, Grille *G, int dim, int nbcl, int aff);

#endif  /* __ENTETE_FONCTIONS__ */
