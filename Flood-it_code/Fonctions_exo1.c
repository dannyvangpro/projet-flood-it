#include<stdio.h>
#include "Entete_Fonctions.h"
#include "time.h"
#include "unistd.h"


/* EXO 1 */

void recherche_case_haut(int **M, int dim, int i, int j, int cl, int *taille, ListeCase *L){
  //cherche vers le haut
	if(i > 0){
		if(M[i-1][j] == cl){
			trouve_zone_rec(M, dim, i-1, j, taille, L);
		}
	}
	return;
}

void recherche_case_bas(int **M, int dim, int i, int j, int cl, int *taille, ListeCase *L){
  //cherche vers le bas 
	if(i+1 < dim){
		if(M[i+1][j] == cl){
			trouve_zone_rec(M, dim, i+1, j, taille, L);
		}
	}
	return;
}

void recherche_case_gauche(int **M, int dim, int i, int j, int cl, int *taille, ListeCase *L){
  //cherche vers la gauche
	if(j > 0){
		if(M[i][j-1] == cl){
			trouve_zone_rec(M, dim, i, j-1 , taille, L);
		}
	}
	return;
}

void recherche_case_droite(int **M, int dim, int i, int j, int cl, int *taille, ListeCase *L){
  //cherche vers la droite
	if(j+1 < dim){
		if(M[i][j+1] == cl){
			trouve_zone_rec(M, dim, i, j+1 , taille, L);
		}
	}
	return;
}

void trouve_zone_rec(int **M, int dim, int i, int j, int *taille, ListeCase *L){
	
	int cl = M[i][j];
	
	/* On ajoute dans L la case i j */
	ajoute_en_tete(L,i,j);
	*taille = *taille + 1;
	M[i][j] = -1;
	
	/* Puis on cherche les cases adjacentes de m�me couleur */
	recherche_case_bas(M, dim, i, j, cl, taille, L);
	recherche_case_gauche(M, dim, i, j, cl, taille, L);
	recherche_case_haut(M, dim, i, j, cl, taille, L);
	recherche_case_droite(M, dim, i, j, cl, taille, L);

	return;
}


int sequence_aleatoire_rec(int **M, Grille *G, int dim, int nbcl, int aff){
  //retourne le nombre de changement necessaire 
	
	/* Choisissons une couleur al�atoire diff�rente de celle de notre Zsg */
	int cl;
	do{
		cl = rand()%nbcl;
	}
	while(cl == M[0][0]);
	
	/* D�terminons notre Zsg */
	ListeCase *L = (ListeCase *)malloc(sizeof(ListeCase));
	init_liste(L);
	int taille = 0;
	
	trouve_zone_rec(M, dim, 0, 0, &taille, L);
	
	/* Condition de victoire */
	if(taille == dim * dim)
		return 0; 
	
	/* Affectons la nouvelle couleur � toutes les cases de notre Zsg */
	int i=0, j=0;
	
	/* Suppression de la liste et attribution de la couleur
	   modification */
	
	while(test_liste_vide(L)==0){
		enleve_en_tete(L, &i, &j);
		M[i][j] = cl;
		if(aff == 1)
			Grille_attribue_couleur_case(G, i, j, cl);
	}
	
	
	/* Actualisons l'affichage */
	if(aff == 1){
		Grille_redessine_Grille(G);
		sleep(1);	
	}
	detruit_liste(L);
	
	return 1 + sequence_aleatoire_rec(M, G, dim, nbcl, aff);
}


